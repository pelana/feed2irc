from pyaib.plugins import plugin_class, every, keyword
from base import Base
import colors
import requests
import speedparser
import requests_cache
import urllib2
requests_cache.configure(backend="memory", expire_after=300)
CONTROL_CHANNEL = "#feed2irc-private"


@plugin_class("rssnyaa")
@plugin_class.requires('db')
class RSS(Base):
    db_name = "rsspluginnyaa"

    def __init__(self, context, config):
        self.db = context.db.get(self.db_name)

    @keyword.channel(CONTROL_CHANNEL)
    @keyword("set_nyaa")
    def add_feed(self, irc_c, msg, trigger, args, kwargs):
        ayuda = """  <#channel> <tid>: set a feed for a channel"""
        tid, channel = args[1], args[0]
        feed_url = "http://www.nyaa.se/?user=%s&page=rss" % tid
        if not "#" in channel:
            msg.reply("#channel format plz")
            return
        if not "http" in feed_url:
            msg.reply("http it is required")
            return
        success = self._add(feed_url, channel)
        if success:
            msg.reply("added url %s to the channel %s" % (feed_url, channel))
            irc_c.JOIN(channel)
            irc_c.PRIVMSG(channel, "Hi im a bot for your nyaa account  %s more"
                                   "info in #feed2irc" % feed_url)
            msg.reply("Joined to %s" % channel)

    @keyword.channel(CONTROL_CHANNEL)
    @keyword("list_nyaa")
    def list_feed(self, irc_c, msg, trigger, args, kwargs):
        ayuda = """ list feeds """
        if len(args) > 0:
            r = self._list(" ".join(args[:]))
        else:
            r = self._list()
        if not r:
            msg.reply("empty")
        for chan, feed in r:
            irc_c.RAW("NOTICE %s :%s" % (msg.nick, "%s %s" % (chan, feed)))

    @keyword.channel(CONTROL_CHANNEL)
    @keyword("reset_nyaa")
    def reset_feed(self, irc_c, msg, trigger, args, kwargs):
        ayuda =""" <#chan> : clear the last feed """
        chan = args[0]
        feed = self._reset(chan)
        msg.reply("reset %s" % feed)

    @keyword.channel(CONTROL_CHANNEL)
    @keyword("del_nyaa")
    def del_feed(self, irc_c, msg, trigger, args, kwargs):
        ayuda =""" <#chan> :: delete a feed and bot parted """
        chan = args[0]
        irc_c.PART(chan, "Bye ...")
        feed, chan = self._delete(chan)
        if chan and feed:
            msg.reply("deleted %s from %s" % (feed, chan))
        else:
            msg.reply("empty")

    @keyword("nyaa")
    @keyword.autohelp
    def search_nyaa(self, irc_c, msg, trigger, args, kwargs):
        """ <search_string> : searching torrent for your nyaa account"""
        channel = msg.channel
        string_search = " ".join(args)
        tids = self._get_tid(channel)
        result =self._search_feed(string_search, tids)
        urls = [ self.get_search_url( string_search,tid, pagetype="search") for tid in tids]
        _urls =" ".join(urls)

        if not result:
            msg.reply("%s result  not found %s " % (msg.nick, _urls))
        for msg_ in result[0:3]:
                irc_c.RAW("NOTICE %s :%s" % (msg.nick, msg_))
        if len(result)>=3:
            irc_c.RAW("NOTICE %s :%s" % (msg.nick, "Too much results to show %s" % _urls))

    @keyword("last_nyaa")
    @keyword.autohelp
    def last_nyaa(self, irc_c, msg, trigger, args, kwargs):
        """ get last nyaa entry"""
        self._reset(msg.channel)
        irc_2 = irc_c
        results = self._get_feed(irc_2, msg.channel, 3)
        msg.reply(" ".join(results))

    def get_search_url(self, search_str, tid, pagetype="rss"):
        url = "http://www.nyaa.se/?user=%s&page=%s&term=%s" % (tid, pagetype,urllib2.quote(search_str))
        return url

    def _search_feed(self,search_str, tids):
        results = []
        for tid in tids:
            url = self.get_search_url(search_str, tid)
            res = requests.get(url).content
            result = speedparser.parse(res)
            entries = result["entries"]
            for entry in entries:
                results.append(self._parse_rss(entry))
        return results


    @every(seconds=15*60)
    def check_feed(self, irc_c, timer_name):
        self._timer_feed(irc_c)

    def _parse_rss(self, entry):
        return self._parse_nyaa(entry)

    def _get_tid(self, channel):
        import urlparse
        res = self._list(channel)
        tids = []
        for chan, feed in res:
            parsed = urlparse.urlparse(feed)
            params = urlparse.parse_qsl(parsed.query)
            d = params[0][1]
            tids.append(d)
        return tids

    def _parse_nyaa(self, entry):
        title = entry["title"]
        download_link = entry["link"]
        view_link = entry["id"]
        idol_view = download_link.replace("download", "idolmaster")
        msg_1 = colors.bold("[Nyaa] %s" % title)

        def underbold(txt):
            return colors.color(colors.underline(colors.bold(txt)), colors.RED)
        info = underbold("Info:")
        msg_2 = "%s %s" % (info, view_link)
        msg_3 = "%s %s" % (underbold("DL:"), download_link)
        msg_4 = ""#"%s %s" % (underbold("Idolm@ster:"), idol_view)
        #msg = "%s @ %s | %s " % (msg_1, msg_2, msg_3) 
        msg = "%s @ %s " % (msg_1, msg_2) 
        return msg


