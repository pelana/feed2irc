import speedparser
from binascii import crc32
import requests
import requests_cache

#requests_cache.configure(backend="memory", expire_after=300)

class Base:

    def _timer_feed(self, irc_c):
        list_rss = self._list()
        results =[]
        for (chan, url) in list_rss:
            results = self._fetch_rss(url, chan)
            for i in results:
                irc_c.PRIVMSG(chan, i)


    def _get_feed(self, irc_c, search=None, number = 0):
        list_rss = self._list(search)
        results =[]
        for (chan, url) in list_rss:
            if number > 0:
                print "fetch url ..."
                res = requests.get(url, verify=False).content
                print url
                print " speedparser ..."
                result = speedparser.parse(res)
                print result
                print "finish ...."
                entries = result["entries"][0:number]
                for entry in entries:
                    results.append(self._parse_rss(entry))
            else:
                res = self._fetch_rss(url, number)
                for i in res:
                    results.append(i)
        return results

    def _fetch_rss(self, url, channel=""):
        crc = crc32(url)
        item = self.db.get("sent%s%s" % (channel,crc))
	#print url
        try:
	    res = requests.get(url).content
        except:
            return []

        result = speedparser.parse(res)
        if len(result["entries"])==0:
            return []
        entry = result["entries"][0]
        if "id" in entry.keys():
            id = "id"
        else:
            id = "link"
        if item.value == entry[id]:
            return []
        item.value =entry[id]
        item.commit()
        return [self._parse_rss(entry), ]

    def _reset(self, chan):
        l = self._list(chan)
        f = None
        for chan, feed in l:
            crc = crc32(feed)
            f = feed
            item = self.db.get("sent%s" % crc)
            item.delete()
        return f

    def _list(self, search_chan=None):
        item = self.db.get("all_records")
        all_records = item.value
        l = []
        if all_records is None:
            return l
        for chan, feed in all_records.items():
            if search_chan is None:
                d = chan, feed
                l.append(d)
            else:
                if search_chan in chan:
                    d = chan, feed
                    l.append(d)
                    break
        return l

    def _delete(self, chan):
        item = self.db.get("all_records")
        all_records = item.value
        if not all_records:
            return None, None
        c = all_records.copy()
        if chan in all_records:
            del all_records[chan]
        item.value = all_records
        item.commit()
        if not chan in c:
            return None, None
        return chan, c[chan]

    def _add(self, url, chan):
        item = self.db.get("all_records")
        all_records = item.value
        if not all_records:
            all_records = {}
        all_records.update({chan: url})
        item.value = all_records
        item.commit()
        return True
