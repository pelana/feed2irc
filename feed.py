from pyaib.plugins import plugin_class, every, keyword
from base import Base
import colors
import urllib
import urlparse


CONTROL_CHANNEL = "#feed2irc-private"


@plugin_class("rss")
@plugin_class.requires('db')
class RSS(Base):
    db_name = "rssplugin"

    def __init__(self, context, config):
        self.db = context.db.get(self.db_name)

    @keyword.channel(CONTROL_CHANNEL)
    @keyword("set_feed")
    def add_feed(self, irc_c, msg, trigger, args, kwargs):
        ayuda = """ <#channel> <url_feed>   : set a feed for a channel"""
        feed_url, channel = args[1], args[0]
        if not "#" in channel:
            msg.reply("#channel format plz")
            return
        if not "http" in feed_url:
            msg.reply("http it is required")
            return
        success = self._add(feed_url, channel)
        if success:
            msg.reply("added url %s to the channel %s" % (feed_url, channel))
            irc_c.JOIN(channel)
            irc_c.PRIVMSG(channel, "Hi im a bot for your rss %s " % feed_url)
            msg.reply("Joined to %s" % channel)

    @keyword.channel(CONTROL_CHANNEL)
    @keyword("list_feed")
    def list_feed(self, irc_c, msg, trigger, args, kwargs):
        ayuda = """ list feeds """
        if len(args) > 0:
            r = self._list(args[0])
        else:
            r = self._list()
        if not r:
            msg.reply("empty")
        for chan, feed in r:
            irc_c.RAW("NOTICE %s :%s" % (msg.nick, "%s %s" % (chan, feed)))

    @keyword.channel(CONTROL_CHANNEL)
    @keyword("reset_feed")
    def reset_feed(self, irc_c, msg, trigger, args, kwargs):
        ayuda = """ <#chan> : clear the last feed """
        chan = args[0]
        feed = self._reset(chan)
        msg.reply("reset %s" % feed)

    @keyword.channel(CONTROL_CHANNEL)
    @keyword("del_feed")
    def del_feed(self, irc_c, msg, trigger, args, kwargs):
        ayuda= """ <#chan> :: delete a feed and bot parted """
        chan = args[0]
        irc_c.PART(chan, "Bye ...")
        feed, chan = self._delete(chan)
        if chan and feed:
            msg.reply("deleted %s from %s" % (feed, chan))
        else:
            msg.reply("empty")

    @keyword("feed")
    def get_feed(self, irc_c, msg, trigger, args, kwargs):
        """ get a feed """
        self._reset(msg.channel)
        results = self._get_feed(irc_c, msg.channel, number=5)
        print "results %s" % len(results)
        for r in results:
            print r
            msg.reply(r)

    @every(seconds=60*3)
    def check_feed(self, irc_c, timer_name):
        self._timer_feed(irc_c)

    def _parse_rss(self, entry):
        title = entry["title"]
        link = entry["link"]
        if "viglink.com" in link:
            l = urllib.unquote(link)
            p = urlparse.urlparse(l)
            link = urlparse.parse_qs(p.query)["u"].pop()
        msg_1 = colors.bold("[RSS] %s" % title)
        info = colors.underline(colors.bold("Info:"))
        msg_2 = "%s" % (colors.color(info, colors.RED))
        msg = "%s @ %s %s" % (msg_1, msg_2, link)
        return msg
